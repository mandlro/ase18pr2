
public class Konto 
{
	// Attribute
	private String inhaber;
	private double kontostand;
	
	public double getKontostand() {
		return kontostand;
	}

	public void setInhaber(String inhaber)	
	{
		this.inhaber = inhaber;
		this.kontostand = 0;
	}
	
	public void setInhaber()	
	{
		this.kontostand = 0;
	}
	
	public void aufbuchen(double betrag)
	{
		kontostand += betrag;
	}
	
	public void abbuchen(double betrag)
	{
		if (kontostand - betrag > 0)
			kontostand -= betrag;
		else
			System.out.println("Betrag ist zu gro�, aktuelles Guthaben: " + kontostand);

	}
	
}
