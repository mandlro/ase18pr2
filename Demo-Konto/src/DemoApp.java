
public class DemoApp {

	public static void main(String[] args) {

		Konto k2 = new Konto();
		k2.setInhaber("Susi Sorglos");
		k2.aufbuchen(500);
		System.out.println("Kontostand von Susi: " + k2.getKontostand());
		
		Konto k = new Konto();
		k.setInhaber("Max Mustermann");
		System.out.println("Kontostand: " + k2.getKontostand());
		
		k.aufbuchen(100);
		System.out.println("Kontostand: " + k2.getKontostand());

		k.abbuchen(120);
		System.out.println("Kontostand: " + k2.getKontostand());

		k.abbuchen(90);
		System.out.println("Kontostand: " + k2.getKontostand());

		System.out.println("Kontostand von Susi: " + k2.getKontostand());
		
	}

}
