package org.campus02;
import java.util.LinkedList;
import java.util.ListIterator;

import org.campus02.student.Student;

public class StudentManager {

	public static void main(String[] args) {

		LinkedList<Student> students = new LinkedList<Student>();
		students.addFirst(new Student("Max"));
		students.addFirst(new Student("Peter"));
		students.addFirst(new Student("John"));
		students.addFirst(new Student("Phil"));
		
		ListIterator<Student> iterator = students.listIterator();
		iterator.next();       // P|JPM
		iterator.next();       // PJ|PM
		iterator.add(new Student("Marie")); // PJM|PM
		iterator.add(new Student("Nena")); // PJMN|PM
		iterator.next(); 		// PJMNP|M
		iterator.remove();	// PJMN|M
		
		iterator = students.listIterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
