
public class ArraysUebung6 {
	public static int zinsen;
	
public static void main(String[] args) {

		
		double[][] matrix = generateIdentityMatrix(5);
		
		printMatrix(matrix);
		
		// sparkasse
		berechneZinsen(10, 10);
		System.out.println("Zinsen:" + zinsen);
		
		// sparkasse
		int endbetrag = summeZinsenFixBetrag(zinsen);
		System.out.println("Summe Zinsen + Fixbetrag " + endbetrag);

	}
	
	public static void printMatrix(double[][] matrix) {
		
		for(int r = 0; r < matrix.length; r++) {
			
			for(int c = 0; c < matrix[r].length; c++) {
				System.out.print(matrix[r][c]+ "\t");
			}
			
			System.out.println();
			
		}
		
	}
	
	public static double[][] generateIdentityMatrix(int size) {
		
		double[][] idMatrix = new double[size][size];
		
		for(int i=0; i < size; i++) {
			idMatrix[i][i] = 1.0;
		}
		
		return idMatrix;
	}
	
	public static void berechneZinsen(int a, int b) {
		zinsen = a * b;
	}
	
	public static int summeZinsenFixBetrag(int zinsen) {
		return zinsen + 10;
	}
}
