
public class TeilerFinden {
	public static void main(String[] args) {

		int zahl = 128;
		findeTeiler(zahl);

	}

	public static void findeTeiler(int zahl) {

		int teilerKandidat = 2;

		String ausgabeAlleTeiler = "teiler von " + zahl + ": ";

		while (teilerKandidat <= zahl / 2) {
			if (zahl % teilerKandidat == 0) {
				ausgabeAlleTeiler += teilerKandidat + ", ";
			}
			teilerKandidat++;
		}

		System.out.println(ausgabeAlleTeiler);

	}

}
